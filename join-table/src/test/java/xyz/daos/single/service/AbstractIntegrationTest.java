package xyz.daos.single.service;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;
import xyz.daos.join.JoinTableApplication;

@SpringBootTest(classes = JoinTableApplication.class)
@Transactional
@Rollback
public abstract class AbstractIntegrationTest {
}
