package xyz.daos.join.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import xyz.daos.join.entity.Buyer;

@Repository
public interface BuyerRepository extends JpaRepository<Buyer, Long> {
}
