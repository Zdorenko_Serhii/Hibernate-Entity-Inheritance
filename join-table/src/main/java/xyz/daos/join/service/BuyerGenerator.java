package xyz.daos.join.service;

import net.bytebuddy.utility.RandomString;
import org.springframework.stereotype.Service;
import xyz.daos.join.entity.BankAccount;
import xyz.daos.join.entity.Buyer;
import xyz.daos.join.entity.CreditCard;

import java.util.ArrayList;

@Service
public class BuyerGenerator {

    public Buyer getRandomBuyer() {
        Buyer buyer = new Buyer();
        buyer.setFirstName(RandomString.make(5));
        buyer.setLastName(RandomString.make(10));

        BankAccount account = new BankAccount();
        account.setAccount(RandomString.make(20));
        account.setBankName(RandomString.make(5));
        account.setBuyer(buyer);
        buyer.setBillingDetails(new ArrayList<>());
        buyer.getBillingDetails().add(account);

        CreditCard card = new CreditCard();
        card.setCardNumber(RandomString.make(20));
        card.setExpYear(2025);
        card.setExpMonth(12);
        card.setBuyer(buyer);
        buyer.getBillingDetails().add(card);
        return buyer;
    }
}
