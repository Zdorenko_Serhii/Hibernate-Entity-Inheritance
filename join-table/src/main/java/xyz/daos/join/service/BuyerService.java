package xyz.daos.join.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import xyz.daos.join.entity.Buyer;
import xyz.daos.join.repository.BuyerRepository;

import java.util.List;

@Service
public class BuyerService {
    @Autowired
    BuyerRepository repository;

    @Transactional(readOnly = true)
    public List<Buyer> getAllBuyers() {
        List<Buyer> all = repository.findAll();
        return all;
    }

    @Transactional
    public Buyer createBuyer(Buyer buyer) {
        return repository.save(buyer);
    }

}
