package xyz.daos.join;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JoinTableApplication {
    public static void main(String[] args) {
        SpringApplication.run(JoinTableApplication.class, args);
    }
}
