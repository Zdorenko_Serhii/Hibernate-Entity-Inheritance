package xyz.daos.single.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import xyz.daos.single.entity.Buyer;
import xyz.daos.single.repository.BuyerRepository;

import java.util.List;

@Service
public class BuyerService {
    @Autowired
    BuyerRepository repository;

    @Transactional(readOnly = true)
    public List<Buyer> getAllBuyers() {
        return repository.findAll();
    }

    @Transactional
    public Buyer createBuyer(Buyer buyer) {
        return repository.save(buyer);
    }

}
