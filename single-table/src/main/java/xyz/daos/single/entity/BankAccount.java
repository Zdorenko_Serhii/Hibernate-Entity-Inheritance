package xyz.daos.single.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Objects;

@Entity
@DiscriminatorValue("bank_account")
public class BankAccount extends BillingDetails {
    String account;
    String bankName;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BankAccount)) return false;
        if (!super.equals(o)) return false;
        BankAccount that = (BankAccount) o;
        return getAccount().equals(that.getAccount()) && getBankName().equals(that.getBankName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getAccount(), getBankName());
    }

    @Override
    public String toString() {
        return "BankAccount{" +
                "account='" + account + '\'' +
                ", bandName='" + bankName + '\'' +
                '}';
    }
}