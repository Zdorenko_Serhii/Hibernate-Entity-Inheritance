package xyz.daos.single.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Objects;

@Entity
@DiscriminatorValue("credit_card")
public class CreditCard extends BillingDetails {
    String cardNumber;
    int expYear;
    int expMonth;

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public int getExpYear() {
        return expYear;
    }

    public void setExpYear(int expYear) {
        this.expYear = expYear;
    }

    public int getExpMonth() {
        return expMonth;
    }

    public void setExpMonth(int expMonth) {
        this.expMonth = expMonth;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CreditCard)) return false;
        if (!super.equals(o)) return false;
        CreditCard that = (CreditCard) o;
        return getExpYear() == that.getExpYear() && getExpMonth() == that.getExpMonth() && getCardNumber().equals(that.getCardNumber());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getCardNumber(), getExpYear(), getExpMonth());
    }

    @Override
    public String toString() {
        return "CreditCard{" +
                "cardNumber='" + cardNumber + '\'' +
                ", expYear=" + expYear +
                ", expMonth=" + expMonth +
                '}';
    }
}
