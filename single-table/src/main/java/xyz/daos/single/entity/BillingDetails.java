package xyz.daos.single.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.STRING)
public class BillingDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    Long id;
    @ManyToOne
    @JoinColumn(name = "buyer_id")
    @JsonBackReference
    Buyer buyer;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Buyer getBuyer() {
        return buyer;
    }

    public void setBuyer(Buyer buyer) {
        this.buyer = buyer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BillingDetails)) return false;
        BillingDetails that = (BillingDetails) o;
        return Objects.equals(getId(), that.getId()) && getBuyer().equals(that.getBuyer());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getBuyer());
    }

    @Override
    public String toString() {
        return "BillingDetails{" +
                "id=" + id +
                ", buyer=" + buyer +
                '}';
    }
}
