package xyz.daos.single.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.daos.single.entity.Buyer;
import xyz.daos.single.service.BuyerGenerator;
import xyz.daos.single.service.BuyerService;

import java.util.List;

@RestController
@RequestMapping("/buyers")
public class BuyerController {
    @Autowired
    BuyerService service;

    @GetMapping
    public ResponseEntity<List<Buyer>> getAll() {
        return new ResponseEntity(service.getAllBuyers(), new HttpHeaders(), HttpStatus.OK);
    }

    @PostMapping("/create")
    public ResponseEntity<Buyer> create(BuyerGenerator generator) {
        return ResponseEntity.ok(service.createBuyer(generator.getRandomBuyer()));
    }
}
