package xyz.daos.single.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import xyz.daos.single.entity.Buyer;

@Repository
public interface BuyerRepository extends JpaRepository<Buyer, Long> {
}
