package xyz.daos.single.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import xyz.daos.single.entity.Buyer;
import xyz.daos.single.repository.BuyerRepository;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class BuyerServiceTestIT extends AbstractIntegrationTest {
    @Autowired
    private BuyerService sut;

    @Autowired
    private BuyerGenerator generator;

    @Autowired
    private BuyerRepository repository;

    @Test
    void shouldFindAllBuyers() {
        //GIVEN
        List<Buyer> buyerToSave = List.of(
                generator.getRandomBuyer(),
                generator.getRandomBuyer());
        repository.saveAll(buyerToSave);

        //WHEN
        List<Buyer> buyersFromDb = sut.getAllBuyers();

        //THEN
        assertArrayEquals(buyerToSave.toArray(), buyersFromDb.toArray());
    }
}